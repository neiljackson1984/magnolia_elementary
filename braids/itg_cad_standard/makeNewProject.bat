@echo off
@rem This script expects to be passed a single parameter on the command line: the path of the project folder that is to be created.

IF [%1] == [] (
	echo. 
	echo Please enter the path of the new project folder that is to be created.
	echo EXAMPLE: J:\Integrated Technologies Group, Inc\skyline elementary
	set /p projectRoot=
) ELSE (
	set projectRoot=%~1
)


echo Creating new project at "%projectRoot%".
@rem to do: refuse to proceed unless either the projectRoot dos not exist or exists and is empty.
mkdir "%projectRoot%"
cd /d "%projectRoot%"
git init

@rem make the standard folders
mkdir delivered & echo > delivered\.empty
mkdir received & echo > received\.empty
mkdir sheets & echo > sheets\.empty
mkdir xref & echo > xref\.empty


@rem create an empty file simply to allow the first commit to be able to happen, because braid will not operate on a repository that has no commits.
echo > .empty 
git add *
git commit -am "initial commit"
cmd /c braid add https://github.com/neiljackson1984/autocad_block_library_utility    braids/autocad_block_library_utility   
cmd /c braid add https://gitlab.com/neiljackson1984/itg_cad_standard                 braids/itg_cad_standard

copy braids\itg_cad_standard\templates\acaddoc-root.lsp        acaddoc.lsp              1>nul 
copy braids\itg_cad_standard\templates\acaddoc-subfolder.lsp   xref\acaddoc.lsp         1>nul 
copy braids\itg_cad_standard\templates\acaddoc-subfolder.lsp   sheets\acaddoc.lsp       1>nul 
copy braids\itg_cad_standard\templates\main_sheet_set.dst      main_sheet_set.dst       1>nul 
copy braids\itg_cad_standard\templates\.gitignore              .gitignore               1>nul
del /F /Q .empty

git add *
git commit -am "project initialized by makeNewProject.bat"

echo. 
echo. 
echo Finished.
echo Now, please manually edit %projectRoot%\main_sheet_set.dst to customize for the current project.
echo.
goto eof




:eof
pause